# appstream-glib-rs
This repository contains the WIP rust bindings for appstream-glib.

## Using
Add this line to your Cargo file
```
[dependencies]
appstream-glib = { git = "https://gitlab.gnome.org/bilelmoussaoui/appstream-glib-rs" }
```
