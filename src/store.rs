/*
use glib::object::IsA;
use glib::translate::*;
use App;
use Store;

pub trait StoreExtManual: 'static {
    fn get_apps_by_id(&self, id: &str) -> Vec<App>;
}




impl<T: IsA<Store>> StoreExtManual for T {
    //#[cfg(any(feature = "v0_5_12", feature = "dox"))]
    
    fn get_apps_by_id(&self, id: &str) -> Vec<App> {
        unsafe {    
            let apps = app_stream_glib_sys::as_store_get_app_by_id(
                self.as_ref().to_glib_none().0,
                id.to_glib_none().0
            );
            FromGlibPtrArrayContainerAsVec::from_glib_full_as_vec(apps)
        }
    }
}

*/