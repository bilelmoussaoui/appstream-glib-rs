// This file was generated by gir (https://github.com/gtk-rs/gir)
// from gir-files (https://github.com/gtk-rs/gir-files)
// DO NOT EDIT

#[cfg(any(feature = "v0_1_4", feature = "dox"))]
use ProblemKind;
use app_stream_glib_sys;
#[cfg(any(feature = "v0_1_4", feature = "dox"))]
use glib::GString;
use glib::object::IsA;
use glib::translate::*;
use std::fmt;

glib_wrapper! {
    pub struct Problem(Object<app_stream_glib_sys::AsProblem, app_stream_glib_sys::AsProblemClass, ProblemClass>);

    match fn {
        get_type => || app_stream_glib_sys::as_problem_get_type(),
    }
}

impl Problem {
    #[cfg(any(feature = "v0_1_4", feature = "dox"))]
    pub fn new() -> Problem {
        unsafe {
            from_glib_full(app_stream_glib_sys::as_problem_new())
        }
    }

    #[cfg(any(feature = "v0_1_4", feature = "dox"))]
    pub fn kind_to_string(kind: ProblemKind) -> Option<GString> {
        unsafe {
            from_glib_none(app_stream_glib_sys::as_problem_kind_to_string(kind.to_glib()))
        }
    }
}

#[cfg(any(feature = "v0_1_4", feature = "dox"))]
impl Default for Problem {
    fn default() -> Self {
        Self::new()
    }
}

pub const NONE_PROBLEM: Option<&Problem> = None;

pub trait ProblemExt: 'static {
    #[cfg(any(feature = "v0_1_4", feature = "dox"))]
    fn get_kind(&self) -> ProblemKind;

    #[cfg(any(feature = "v0_1_4", feature = "dox"))]
    fn get_line_number(&self) -> u32;

    #[cfg(any(feature = "v0_1_4", feature = "dox"))]
    fn get_message(&self) -> Option<GString>;

    #[cfg(any(feature = "v0_1_4", feature = "dox"))]
    fn set_kind(&self, kind: ProblemKind);

    #[cfg(any(feature = "v0_1_4", feature = "dox"))]
    fn set_line_number(&self, line_number: u32);

    #[cfg(any(feature = "v0_1_4", feature = "dox"))]
    fn set_message(&self, message: &str);
}

impl<O: IsA<Problem>> ProblemExt for O {
    #[cfg(any(feature = "v0_1_4", feature = "dox"))]
    fn get_kind(&self) -> ProblemKind {
        unsafe {
            from_glib(app_stream_glib_sys::as_problem_get_kind(self.as_ref().to_glib_none().0))
        }
    }

    #[cfg(any(feature = "v0_1_4", feature = "dox"))]
    fn get_line_number(&self) -> u32 {
        unsafe {
            app_stream_glib_sys::as_problem_get_line_number(self.as_ref().to_glib_none().0)
        }
    }

    #[cfg(any(feature = "v0_1_4", feature = "dox"))]
    fn get_message(&self) -> Option<GString> {
        unsafe {
            from_glib_none(app_stream_glib_sys::as_problem_get_message(self.as_ref().to_glib_none().0))
        }
    }

    #[cfg(any(feature = "v0_1_4", feature = "dox"))]
    fn set_kind(&self, kind: ProblemKind) {
        unsafe {
            app_stream_glib_sys::as_problem_set_kind(self.as_ref().to_glib_none().0, kind.to_glib());
        }
    }

    #[cfg(any(feature = "v0_1_4", feature = "dox"))]
    fn set_line_number(&self, line_number: u32) {
        unsafe {
            app_stream_glib_sys::as_problem_set_line_number(self.as_ref().to_glib_none().0, line_number);
        }
    }

    #[cfg(any(feature = "v0_1_4", feature = "dox"))]
    fn set_message(&self, message: &str) {
        unsafe {
            app_stream_glib_sys::as_problem_set_message(self.as_ref().to_glib_none().0, message.to_glib_none().0);
        }
    }
}

impl fmt::Display for Problem {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Problem")
    }
}
